﻿using Extension;
using OSS.Lib;
using OSS.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace LayerTo1C._1C
{
    class LinqTo1C
    {
        private const string pathToWorkDir = "C:\\www\\layer1C\\";
        private static dynamic conn1C;

        public static dynamic Conn1C { get { return conn1C = _1CWorker.start1C(conn1C); } }


        dynamic Запрос;
        dynamic Выборка;

        Type v82ComConnector = Type.GetTypeFromProgID("V83.COMConnector");

        public void CreateQuery1C()
        {

            Object[] ar = { "Запрос" };
            Запрос = v82ComConnector.InvokeMember("NewObject", BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static, null, Conn1C, ar);

        }

        public void SetParam(string name, object value)
        {
            Запрос.Параметры.Вставить(name, value);
        }

        public dynamic GetResultValue(string queryText)
        {

            //СД.ЭтоГруппа И
            Запрос.Текст = queryText;
            Выборка = Запрос.Выполнить.Выбрать();
            return Выборка;

            //Object[] ar = { };
            //dynamic _Docs = v82ComConnector.InvokeMember("Документы", BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static, null, result, ar);
            //dynamic b = _Docs.Т_АктОказанияУслугПоОтправкеПриемуКонтейнеров.НайтиПоНомеру(akt, date);
            //b = b.ПолучитьОбъект();
            //b.СохранитьРасшифровку(DecodeFileName);
        }

        public dynamic GetFileByUIDDoc(string GUID)
        {
            return null;
        }

        public decimal GetPriceServiceFrom1C(string KodeService)
        {
            decimal Price = 0;
            string Query = @"ВЫБРАТЬ  
               ЦеныНоменклатурыСрезПоследних.Цена как ЦенаУслуги  
                
 ИЗ    Справочник.Номенклатура КАК Номенклатура1  
 ЛЕВОЕ СОЕДИНЕНИЕ 
                РегистрСведений.ЦеныНоменклатуры.СрезПоследних(&Период) КАК ЦеныНоменклатурыСрезПоследних  ПО ЦеныНоменклатурыСрезПоследних.ТипЦен.Код = ""00000001 "" 
И ЦеныНоменклатурыСрезПоследних.Номенклатура = Номенклатура1.Ссылка  
ГДЕ  
                (Номенклатура1.Код = &КОД)
 ";
            // Query = @"ВЫБРАТЬ СД.Ссылка КАК Ссылка, СД.Наименование КАК Наименование ИЗ Справочник.Д_СогласованиеДокументов КАК СД ГДЕ  СД.Родитель.Ссылка ЕСТЬ NULL ";
            this.CreateQuery1C();
            this.SetParam("КОД", KodeService);
            this.SetParam("Период", DateTime.Now.Date);

            var Выборка = this.GetResultValue(Query);

            while (Выборка.Следующий())
            {
                Price = Выборка.ЦенаУслуги;
            }
            return Price;
        }
        public string GeneratePriorBill(int bid_id, DateTime DateOutput)
        {
            XMLConfig.Load();
            DataBase.Connect();
#if DEBUG

            V8.RunApp();
#else
            V8.App = Conn1C;
            var prop = typeof(V8).GetField("Cls", BindingFlags.Static |
                          BindingFlags.NonPublic);
            prop.SetValue(null, v82ComConnector);
#endif
            var checkValid = true;

            BidPays pays = new BidPays(bid_id);
            if (pays.PayedSum > 0)//есть оплаты
                checkValid = false;

            BidServices servs = new BidServices(bid_id, false);
            if (servs.ActUid != "")//есть акт
                checkValid = false;

            var cmrsServices = BidsServicesProcess.FillCmrServices(bid_id, DateOutput);
            if (cmrsServices.Where(x => x.whCmrOpList.Where(o => o.OperationType == "partial").Count() > 0).Count() > 0)//есть акт
                checkValid = false;


            if (checkValid)
            {
                BidsServicesProcess.FillBidServices(bid_id, DateOutput);
                User.WriteLog("Заполнены или обновлены услуги в заявке " + bid_id);
                BidsServicesProcess.GenerateBillForBid(bid_id, DateOutput);
                string logMsg = $"Сформированы заявка {bid_id} и счёт(а) в 1С.";
                User.WriteLog(logMsg);
            }

            servs = new BidServices(bid_id, false);
#if DEBUG
            V8.ExitApp();
#endif
            return servs.BillUid;

        }

        public byte[] GetFileUID(Doc1CPoint _doc1CPoint)
        {
            var ArrayByte = new byte[] { };
            var NullByte = new byte[] { 0, 0, 0, 0 };
            var uidDoc = (_doc1CPoint.IsPrior ? GeneratePriorBill(int.Parse(_doc1CPoint.BidId), _doc1CPoint.CalculDate) : _doc1CPoint._guid.ToString());

            Object[] ar = { "УникальныйИдентификатор", uidDoc };

            dynamic UniqIDObj = null;
            try
            {
                UniqIDObj = v82ComConnector.InvokeMember("NewObject", BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static, null, Conn1C, ar);
            }
            catch { return NullByte; }
            ar = new Object[] { };
            dynamic Документы = v82ComConnector.InvokeMember("Документы", BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static, null, Conn1C, ar);

            string FileName = pathToWorkDir + "1C_" + uidDoc + ".xls";
            string file_name_PDF = Path.GetDirectoryName(FileName) + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".pdf";
            var excelPDF = new ExcelJPGMerge();

            var ListSign = new List<SignInTo>();
            if (_doc1CPoint.typeDoc == 1)
            {

                dynamic Doc1c = Документы.СчетНаОплатуПокупателю.ПолучитьСсылку(UniqIDObj);

                try { Doc1c = Doc1c.ПолучитьОбъект(); } catch { return ArrayByte; }

                if (_doc1CPoint.IsCheck)
                    return NullByte;
                ListSign.Add(new SignInTo()
                {
                    SignName = pathToWorkDir + "Sign.png",
                    FieldLandMark = "Руководитель",
                    offsetX = 85,
                    offsetY = -23
                });
                ListSign.Add(new SignInTo()
                {
                    SignName = pathToWorkDir + "Sign.png",
                    FieldLandMark = "Бухгалтер",
                    offsetX = 85,
                    offsetY = -23
                });
                Doc1c.СохранитьСчет(FileName);

            }
            if (_doc1CPoint.typeDoc == 2)
            {
                dynamic Doc1c = Документы.Т_АктОказанияУслугПоОтправкеПриемуКонтейнеров.ПолучитьСсылку(UniqIDObj);

                try { Doc1c = Doc1c.ПолучитьОбъект(); } catch { return ArrayByte; }

                if (_doc1CPoint.IsCheck)
                    return NullByte;
                ListSign.Add(new SignInTo()
                {
                    SignName = pathToWorkDir + "Sign.png",
                    FieldLandMark = "Комментарий:",
                    offsetX = 70,
                    offsetY = 45
                });
                ListSign.Add(new SignInTo()
                {
                    FieldLandMark = "Комментарий:",

                    SignName = pathToWorkDir + "stamp.png",
                    offsetX = 70,
                    offsetY = 95
                });
                Doc1c.СохранитьАвтоСВХ(FileName);
            }
            if (_doc1CPoint.typeDoc == 3)
            {
                dynamic Doc1c = Документы.СчетФактураВыданный.ПолучитьСсылку(UniqIDObj);

                try { Doc1c = Doc1c.ПолучитьОбъект(); } catch { return ArrayByte; }

                if (_doc1CPoint.IsCheck)
                    return NullByte;
                ListSign.Add(new SignInTo()
                {
                    SignName = pathToWorkDir + "Sign.png",
                    FieldLandMark = "Руководитель",
                    OrientLandscape = true,
                    offsetX = 140,
                    offsetY = -13
                });
                ListSign.Add(new SignInTo()
                {
                    SignName = pathToWorkDir + "Sign.png",
                    FieldLandMark = "Главный",
                    OrientLandscape = true,
                    offsetX = 120,
                    offsetY = -13
                });
                Doc1c.СохранитьСчетФактуру(FileName);
            }
            if (_doc1CPoint.typeDoc == 1 || _doc1CPoint.typeDoc == 2 || _doc1CPoint.typeDoc == 3)
            {
                if (_doc1CPoint.inExcelStyle)
                    ArrayByte = File.ReadAllBytes(FileName);
                else
                {
                    excelPDF.Convert(FileName, file_name_PDF, ListSign);
                    ArrayByte = File.ReadAllBytes(file_name_PDF);
                    File.Delete(file_name_PDF);
                }
                File.Delete(FileName);
            }
            if (_doc1CPoint.typeDoc == 4)
            {
                XMLConfig.Load();
                DataBase.Connect();


                V8.App = Conn1C;

                var prop = typeof(V8).GetField("Cls", BindingFlags.Static |
                              BindingFlags.NonPublic);
                prop.SetValue(null, v82ComConnector);

                if (_doc1CPoint.IsCheck)
                    return NullByte;
                var bidId = int.Parse(_doc1CPoint.BidId);
                var ctrId = int.Parse(_doc1CPoint.CtrId);
                //var _BillsInfo = new Bills1C(bidId, ctrId);
                //var _BillsInfo = new BalansETT;
                var enc = Encoding.UTF8;
                //ArrayByte = enc.GetBytes(_BillsInfo.SumCanBeAssigned.ToString());
                ArrayByte = enc.GetBytes(BalansETT(ctrId).ToString());
                
            }

            return ArrayByte;
        }

        public decimal BalansETT(int CtrId)
        {
            dynamic Contract1C = V8.GetContract(OSS.Utils.Value.ToStr(CtrId));
            decimal SumCanBeAssigned = OSS.Utils.Value.ToDecimal(V8.GetModule("ИнтеграцияПризма").ПолучитьСвободнуюОплату(Contract1C)); // PrepaymentSum + BidPayments - AssignedSumClient;//
            return SumCanBeAssigned;
        }
    }
    
}
