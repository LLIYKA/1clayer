﻿using OSS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace LayerTo1C
{
    class LayerService : ServiceBase
    {
        public static bool STOP { get; set; }
        public LayerService()
        {
            this.ServiceName = "LayerTo1C_Service";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;
        }

        protected override void OnStart(string[] args)
        {
            // TODO: add startup stuff
            var Listner = new WatcherPort();
            var MainTask = new Task(Listner.Run);
            MainTask.Start();


        }

        protected override void OnStop()
        {
            STOP = true;
            V8.ExitApp();
            // TODO: add shutdown stuff
        }
    }
}
