﻿using LayerTo1C._1C;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LayerTo1C
{
  class WatcherPort
  {
    public static ManualResetEvent allDone = new ManualResetEvent(false);

    public void Run()
    {
      Thread.Sleep(20000);
      var CallConnectTo1C = LinqTo1C.Conn1C;
      var ServerSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      var Ip = IPAddress.Parse("127.0.0.1");
      var _endPoint = new IPEndPoint(Ip, 1234);
      ServerSock.Bind(_endPoint);
      ServerSock.Listen(100);


      while (!LayerService.STOP)
      {
        allDone.Reset();
        ServerSock.BeginAccept(new AsyncCallback(AsyncBeginAccep), ServerSock);
        allDone.WaitOne();
      }
    }


    protected void AsyncBeginAccep(IAsyncResult InputRes)
    {
      allDone.Set();
      var _dataRecip = new DataRecip(((Socket)InputRes.AsyncState).EndAccept(InputRes));

      _dataRecip._socket.BeginReceive(_dataRecip.BUFer, 0, _dataRecip.BUFer.Count(), SocketFlags.None, new AsyncCallback(AsyncBeginRecive), _dataRecip);

    }
    protected void AsyncBeginRecive(IAsyncResult InputRes)
    {
      var _dataRecip = (DataRecip)InputRes.AsyncState;
      int CountByte = _dataRecip._socket.EndReceive(InputRes);

      if (CountByte > 0)
      {
        _dataRecip.StoreData();
        var RecipStr = Encoding.UTF8.GetString(_dataRecip.StoredArray);
        if (RecipStr.IndexOf("<EOF>") > -1)
        {
          RecipStr = RecipStr.Substring(0, RecipStr.IndexOf("<EOF>"));
          var _doc1CPoint = DSeriLz(Encoding.UTF8.GetBytes(RecipStr));
          var _linqTo1C = new LinqTo1C();

          var BufResponseArr = _linqTo1C.GetFileUID(_doc1CPoint);
          var ArrayEOF = Encoding.UTF8.GetBytes("<EOF>");
          var ResponseArray = new byte[BufResponseArr.Count() + ArrayEOF.Count()];

          BufResponseArr.CopyTo(ResponseArray, 0);
          ArrayEOF.CopyTo(ResponseArray, BufResponseArr.Count());
          _dataRecip._socket.Send(ResponseArray);
        }
        else
        {
          _dataRecip._socket.BeginReceive(_dataRecip.BUFer, 0, _dataRecip.BUFer.Count(), SocketFlags.None, new AsyncCallback(AsyncBeginRecive), _dataRecip);
        }
      }
    }

    protected Doc1CPoint DSeriLz(byte[] SourceStr)
    {
      MemoryStream streamSeriLz = new MemoryStream(SourceStr);

      streamSeriLz.Position = 0;
      DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Doc1CPoint));

      Doc1CPoint _D1CPoint = (Doc1CPoint)ser.ReadObject(streamSeriLz);
      return _D1CPoint;
    }
  }
  [DataContract]
  class Doc1CPoint
  {
    public Doc1CPoint(int typeDoc, Guid _guid)
    {
      this.typeDoc = typeDoc;
      this._guid = _guid;
    }
    public Doc1CPoint(string typeDocStr, Guid _guid)
    {
      this.typeDoc = RetrivetypeDoc(typeDocStr);
      this._guid = _guid;
    }
    [DataMember]
    public int typeDoc { get; set; }
    [DataMember]
    public Guid _guid { get; set; }
    [DataMember]
    public bool IsCheck { get; set; }
    [DataMember]
    public string BidId { get; set; }
    [DataMember]
    public string CtrId { get; set; }
    [DataMember]
    public DateTime CalculDate { get; set; }
    [DataMember]
    public bool IsPrior { get; set; }
    [DataMember]
    public bool inExcelStyle { get; set; }

    public int RetrivetypeDoc(string FiledName)
    {
      FiledName = FiledName.ToLower();
      int Type_id = 1;
      if (FiledName.Contains("bill"))
      {
        Type_id = 1;
      }
      if (FiledName.Contains("act"))
      {
        Type_id = 2;
      }
      if (FiledName.Contains("invoice"))
      {
        Type_id = 3;
      }
      if (FiledName.Contains("SumCanBeAssigned"))
      {
        Type_id = 4;
      }
      return Type_id;
    }
  }
  class DataRecip
  {
    public byte[] BUFer { get; set; } = new byte[1024];

    public void StoreData()
    {
      var countItem = StoredArray.Count() + BUFer.Count();
      var ResizeArray = new byte[countItem];
      StoredArray.CopyTo(ResizeArray, 0);
      BUFer.CopyTo(ResizeArray, StoredArray.Count());
      StoredArray = ResizeArray;
    }
    public byte[] StoredArray { get; set; } = new byte[0];
    public Socket _socket { get; set; }
    public DataRecip(Socket _socket)
    {

      this._socket = _socket;
    }
  }
}
